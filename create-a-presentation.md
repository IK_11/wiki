# How to create a presentation in the community portal

The community portal [hosts](https://community.torproject.org/training/resources/) a number of training resources in various formats.

If you want to add a new training resource you can link your new resource in:
- https://gitlab.torproject.org/tpo/web/community/-/tree/master/content/training/resources

The community portal also hosts a template to create presentation in html with [RevealJS](https://revealjs.com/)

To create a new slideshow, create a new folder under `content/training/resources`, then add a `contents.lr`.

The `contents.lr` file should use the `slideshow` model and the `slideshow.html` template. It should also define a title for the presentation:

```
_model: slideshow
---
title: The Tor Network
---
_template: slideshow.html
---
background: white
---
image: /static/images/onion.png

```

The image field is used as a small watermark on the bottom right side of the slides.

In the same file you can also define all the different slides under the `slides` field:

```
slides:

#### slide ####
title: Topics
----
description:

- What' s Tor?
```

Every slide should start with a `#### slide ####` line and can contain a title, a description and/or an image. Images and medias can also be added in the description via markdown or via the lektor edit interface via a button that can attach the image.

If you want to add a title slide you can do so with the following piece of code:
```
#### slide ####
title: Types of Relays
----
slide_layout: title-slide
----
background_image: /static/images/onion-white.png
----
description:
-
----
```
The `slide_layout` field is used to define the title-slide. With time we can add more slide layouts!
The `background_image` is used to add a small watermark on the bottom right side.

Unfortunately the current version of lektor on the build machines do need an empty `description` field like in the snippet provided above.

A title slide can also define an `author` field and a `subtitle`, ex:
```
#### slide ####
title: Thank you!
----
author:
name - email@example.com
----
subtitle:
PGP FINGERPRINT
----
slide_layout: title-slide
----
background_image: /static/images/onion-white.png
----
description:
-
```

The slideshow can scroll only from top to bottom. This way we are able to provide compatibility with both left-to-right and right-to-left languages.

If the slides are being localized, the order class is being applied based on the order defined in the `alternatives.ini` databag.
