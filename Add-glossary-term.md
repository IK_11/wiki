The Glossary needs to be updated with new terms and definitions. To add a new term, after updating your git repository, create a folder called like the term, in the directory `content/glossary/`:

`$ cd content/glossary && mkdir Snowflake`

Inside this directory, we create a file called contents.lr, with this template:

```
_model: word
---
term: Snowflake <-- change it to the word we are defining
---
definition:

Here we can write markdown following the documentation at https://gitlab.torproject.org/torproject/web/tpo/-/wikis/Writing-the-content
 
---
translation: Here we add translation notes, if any.
---
spelling: here we can add spelling notes.  
