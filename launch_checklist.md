# Launch Checklist

These are the steps that we need to go through in order to successfully launch a new Tor Project website or portal.

## 1 month prior to launch

- [ ] Launch full portal on staging
- [ ] Set aside some time for a core set of people to fully test the website
    - [ ] On different devices/screen sizes
    - [ ] Different language sets - depending on what's been localised
    - [ ] Clicking through all page links
- [ ] Open issues on trac with findings
- [ ] Freeze all strings and content ready to be sent for localization

## 2 weeks prior to launch

- [ ] Ensure all tickets open during testing are closed
- [ ] Decide on languages for launch based on completion percentage
- [ ] Make a list of links for the new website
- [ ] Start working on launch announcement blog post

## 1 day before launch

- [ ] Final checks as above
    - [ ] Blog post reviewed and ready to publish
- [ ] Inform people (tor-internal) that this is going to happen
  - [ ] how to respond to users
  - [ ] how to report bugs
  - [ ] how to set right expectations about timelines for fixes
 
## Steps During Launch

- [ ] Run jenkins job (if necessary)
- [ ] Update .htaccess for redirects (if necessary)
- [ ] Run a crawler to check for broken links 

## Day After Launch

- [ ] Press release, blog, twitter, etc...
- [ ] Monitor for broken links, redirects, etc...
- [ ] Monitor blog comments for issues - community team 
- [ ] Monitor irc and deal with comments, issue reports, etc...
- [ ] Monitor Apache 404 logs for broken links
- [ ] Explain how to report issues/bugs/broken links and set expectations for time to fix